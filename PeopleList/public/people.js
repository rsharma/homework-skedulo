fetch("http://localhost:3000/people")
	.then((response) => response.json())
	.then((people) => {
		var ids = people.map(p =>p.id);
		Promise.all([
		  fetch("http://localhost:3000/skills?personIds="+ids.toString()).then((response) => response.json()),
		  fetch("http://localhost:3000/interests?personIds="+ids.toString()).then((response) => response.json()),
		  fetch("http://localhost:3000/richest").then((response) => response.json())
		]).then(([skills, interests, richest]) => {
			var dom = document.getElementById("people-list")
			people.map( p => {
				var div = document.createElement("div");
				div.className = (richest.richestPerson == p.id)?"divTableRow bold":"divTableRow";
				div.innerHTML = `<div class="divTableCell">${p.name}</div>
								 <div class="divTableCell">${p.org}</div>
								 <div class="divTableCell">${skills.filter(s => s.personId == p.id).map(s=> s.name).join(", ")}</div>
								 <div class="divTableCell">${interests.filter(s => s.personId == p.id).map(s=> s.name).join(", ")}</div>`;
				dom.appendChild(div);
			})
		})
	})
	.catch((error) => {
	  console.log(error)
	});