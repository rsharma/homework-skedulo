## Question 1: Check divisible by modulo operator

## Question 2: Order Product
        - Fetch 100 products at a time.
        - To avoid N+1 queries, fetched products by product ids.
        - Since function must be able to handle up to 100 records at a time, the API get maximum of 100 products. 

## Question 5: The sound of music
   - All events are sorted by start time first
   - Maintained a priority queue based on Priority of events
   - Time quantum is 1 min
   - Preempted when there is higher priority event at same time