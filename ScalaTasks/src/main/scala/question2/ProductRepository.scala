package question2

import slick.driver.PostgresDriver.api._
import util.DBConfig
import scala.concurrent.Future

/**
 * Created on 12/13/18.
 */
object ProductRepository {

  /**
   * Product table to map database schema
   * @param tag
   */
  class Products(tag: Tag) extends Table[Product](tag, "products") {
    def productId = column[Int]("product_id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name")

    def price = column[Float]("price")

    def * = (productId, name, price) <> ((Product.apply _).tupled, Product.unapply)
  }

  val products = TableQuery[Products]

  /**
   * This method is used to get products by list of ids to avoid N+1 Queries
   *
   * @param productIds arguments
   * @return
   */
  def fetchProductsByIds(productIds: Iterable[Int]): Future[Seq[Product]] = {
    val query = ProductRepository.products.filter(_.productId inSet productIds).result
    DBConfig.db.run(query)
  }
}
