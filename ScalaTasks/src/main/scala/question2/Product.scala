package question2

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created on 12/13/18.
 */
case class Product(productId: Int, name: String, price: Float)

case class OrderProduct(orderId: Int, productId: Int, name: Option[String] = None, price: Option[Float] = None)

object Product{
  /**
   * Get products from database based on the order products provided.
   * To avoid N+1 Queries, products are fetched by ids in single query.
   * Process 100 products at a time
   *
   * @param orderProducts
   * @return
   */
  def getOrderProducts(orderProducts: Array[OrderProduct]): Future[Iterable[OrderProduct]] = {
    val productMap = orderProducts.groupBy(_.productId)
    val productIdsList = productMap.keys.grouped(100)

    val maybeProducts = Future.sequence(productIdsList.map(t=> ProductRepository.fetchProductsByIds(t)))
    for{
      productsList <- maybeProducts
    } yield {
      val products = productsList.toSeq.flatten
      productMap.flatMap{t=>
        products.find(_.productId == t._1) match {
          case Some(p) =>
            t._2.map(_.copy(name = Some(p.name), price = Some(p.price)))
          case _ =>
            t._2
        }
      }
    }
  }
}
