package question1

/**
 * Created on 12/13/18.
 */
object NumberPrinter extends App{

  1 to 100 map filter foreach println

  def filter(n: Int) = {
    if(n%5==0 && n%4==0) "HelloWorld" else if (n%5==0) "World" else if (n%4 ==0) "Hello" else n
  }
}
