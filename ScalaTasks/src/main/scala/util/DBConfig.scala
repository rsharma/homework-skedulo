package util

import com.typesafe.config.ConfigFactory
import slick.driver.PostgresDriver.api._

/**
 * Created on 12/14/18.
 */
object DBConfig {

  val config = ConfigFactory.load
  val url = config.getString("postgres.url")
  val user = config.getString("postgres.user")
  val pass = config.getString("postgres.password")
  val db = Database.forURL(url, user, pass)

}
