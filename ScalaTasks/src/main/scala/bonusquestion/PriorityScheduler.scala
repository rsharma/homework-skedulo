package bonusquestion

import org.joda.time.{Minutes, DateTime}

import scala.annotation.tailrec
import scala.collection.mutable

/**
 * Created on 12/14/18.
 */
object PriorityScheduler {

  implicit def dateTimeOrdering: Ordering[DateTime] = Ordering.fromLessThan(_ isBefore _)

  def optimalSchedule(performances: Array[Performance]): List[Schedule] = {
    if(performances.isEmpty) Nil
    else {
      //1. Sort the array by start time and priority
      val sortedList = performances.sortWith(_.sortPerformance(_)).toList

      //2. Initialize time as start time
      val initialTime = sortedList.head.start

      val priorityQueue = mutable.PriorityQueue.empty[Performance](Ordering.by(t => t.priority))

      @tailrec
      def iterate(input: List[Performance], quantum: DateTime, acc: List[Schedule]): List[Schedule] = input match {
        case Nil => acc
        case xs =>
          //Check arrival of new performance at this point, insert into queue
          input.filter(t => t.canBeScheduled(quantum)).foreach(t => priorityQueue.enqueue(t))

          if (priorityQueue.nonEmpty) {
            //Get the highest priority from the queue.
            val performance = priorityQueue.dequeue()
            val schedule = Schedule(performance.band, quantum, performance.priority, quantum.plusMinutes(Schedule.timeQuantum))
            //Schedule the high priority performance for 1 min. If more than 1 min, put it in queue and iterate
            if (Minutes.minutesBetween(quantum.plusMinutes(Schedule.timeQuantum), performance.finish).getMinutes >= 0) {
              priorityQueue.dequeueAll
              //priorityQueue.enqueue(performance)
              iterate(xs, quantum.plusMinutes(Schedule.timeQuantum), acc :+ schedule)
            } else{
              iterate(xs.tail, quantum.plusMinutes(Schedule.timeQuantum), acc)
            }
          } else {
            iterate(xs.tail, quantum.plusMinutes(Schedule.timeQuantum), acc)
          }
      }

      iterate(sortedList, initialTime, Nil)
    }
  }
}
