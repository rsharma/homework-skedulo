package bonusquestion

import org.joda.time.{DateTime, Minutes}

case class Performance(band: String, start: DateTime, finish: DateTime, priority: Int) {

  //Sort performance by start time. If time are equal then by priority
  def sortPerformance(that: Performance) = {
    if(this.start == that.start)
      this.priority > that.priority
    else this.start isBefore  that.start
  }

  //Performance can be scheduled if time quantum is between the the start and end time
  def canBeScheduled(quantum: DateTime): Boolean =
    Minutes.minutesBetween(this.start, quantum).getMinutes >= 0 &&
    Minutes.minutesBetween(quantum.plusMinutes(Schedule.timeQuantum), this.finish).getMinutes >= 0

}
