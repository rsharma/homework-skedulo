package bonusquestion

import org.joda.time.DateTime

case class Schedule(band: String, time: DateTime, priority: Int, end: DateTime)

object Schedule {
  //Time quantum for minute by minute basis
  val timeQuantum = 1
}