package bonusquestion

/**
 * Created on 12/19/18.
 */
object MusicFestival {

  def getSchedule(performances: Array[Performance]): Seq[Option[Schedule]] = {

    val schedules = PriorityScheduler.optimalSchedule(performances)

    //Display schedule to Sally
    def prepareSchedules(l: List[Schedule], acc: Seq[Option[Schedule]] = Nil, previous: Option[Schedule] = None): Seq[Option[Schedule]] = l match {
      case Nil => if (previous.isEmpty) acc else acc :+ previous
      case x :: xs =>
        if (previous.isEmpty)
          prepareSchedules(xs, acc, Some(x))
        else if (previous.get.band.equals(x.band)) {
          prepareSchedules(xs, acc, Some(previous.get.copy(end = x.end)))
        } else {
          prepareSchedules(xs, acc :+ previous, Some(x))
        }
    }

    prepareSchedules(schedules)
  }
}
