package question2

import org.scalatest.{AsyncFlatSpec, Matchers}

/**
 * Created on 12/14/18.
 */
class ProductSpec extends AsyncFlatSpec with Matchers {

  "Assuming product is already in database, it" should "get product price and name" in {
    val orderProducts = (1 to 1000).flatMap(t=> Array(OrderProduct(1, t))).toArray

    val newOrderProducts = Product.getOrderProducts(orderProducts)
    newOrderProducts.map(p => assert(p.nonEmpty))
    newOrderProducts.map(p => assert(p.size == orderProducts.length))
  }

  "When empty order provided" should "it should not fetch from database" in {
    val orderProducts = Array.empty[OrderProduct]

    val newOrderProducts = Product.getOrderProducts(orderProducts)
    newOrderProducts.map(p => assert(p.nonEmpty))
    newOrderProducts.map(p => assert(p.size == orderProducts.length))
  }

  "List provided more than 200000 orders" should "complete execution" in {
    val orderProducts = (1 to 200000).flatMap(t=> Array(OrderProduct(1, t))).toArray

    val newOrderProducts = Product.getOrderProducts(orderProducts)
    newOrderProducts.map(p => assert(p.nonEmpty))
    newOrderProducts.map(p => assert(p.size == orderProducts.length))
  }
}
