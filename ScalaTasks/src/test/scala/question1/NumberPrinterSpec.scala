package question1

import org.scalatest._

/**
 * Created on 12/13/18.
 */
class NumberPrinterSpec extends FlatSpec with Matchers {

  "Divisible by 4" should "return Hello" in {
    NumberPrinter.filter(4) should be ("Hello")
  }

  "Divisible by 5" should "return World" in {
    NumberPrinter.filter(5) should be ("World")
  }

  "Divisible by both 4 and 5" should "return HelloWorld" in {
    NumberPrinter.filter(20) should be ("HelloWorld")
  }

  "All other number" should "return same number" in {
    NumberPrinter.filter(1) should be (1)
    NumberPrinter.filter(99) should be (99)
  }
}
