package bonusquestion

import org.joda.time.DateTime
import org.scalatest.{Matchers, FlatSpec}

/**
 * Created on 12/14/18.
 */
class MusicFestivalSpec extends FlatSpec with Matchers {

  info("Bonus Question: Scheduler")

  "First and last band" should "be Scorpions" in {
    val performances = Array(
      Performance("Scorpions",    new DateTime(2018, 1, 1, 1, 9), new DateTime(2018, 1, 1, 1, 11), 4),
      Performance("LedZeppelin",  new DateTime(2018, 1, 1, 1, 3), new DateTime(2018, 1, 1, 1, 7), 5),
      Performance("Megadeath",    new DateTime(2018, 1, 1, 1, 5), new DateTime(2018, 1, 1, 1, 7), 6),
      Performance("Metallica",    new DateTime(2018, 1, 1, 1, 2), new DateTime(2018, 1, 1, 1, 4), 2),
      Performance("PinkFloyd",    new DateTime(2018, 1, 1, 1, 7), new DateTime(2018, 1, 1, 1, 8), 10),
      Performance("Motorhead",    new DateTime(2018, 1, 1, 1, 3), new DateTime(2018, 1, 1, 1, 6), 10))

    val schedules = MusicFestival.getSchedule(performances)

    assert(schedules.head.get.band == "Metallica")
    assert(schedules.last.get.band == "Scorpions")
  }

  "Empty Array " should "return Nil" in {
    assert(PriorityScheduler.optimalSchedule(Array.empty) == Nil)
  }

  "All with same start time" should "schedule highest priority event" in {
    val performances = Array(
      Performance("Scorpions",    new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 3), 4),
      Performance("LedZeppelin",  new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 2), 5),
      Performance("Megadeath",    new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 5), 6),
      Performance("Metallica",    new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 4), 2),
      Performance("PinkFloyd",    new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 3), 10),
      Performance("Motorhead",    new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 6), 9))

    val schedules = MusicFestival.getSchedule(performances)

    assert(schedules.head.get.band == "PinkFloyd")
    assert(schedules.last.get.band == "Motorhead")
  }

  "All with same start, end and priority" should "be schedule in FCFS" in {
    val performances = Array(
      Performance("Scorpions",    new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 3), 4),
      Performance("LedZeppelin",  new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 3), 5),
      Performance("Megadeath",    new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 3), 6),
      Performance("Metallica",    new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 3), 2),
      Performance("PinkFloyd",    new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 3), 10),
      Performance("Motorhead",    new DateTime(2018, 1, 1, 1, 1), new DateTime(2018, 1, 1, 1, 3), 10))

    val schedules = MusicFestival.getSchedule(performances)

    assert(schedules.head.get.band == "PinkFloyd")
    assert(schedules.last.get.band == "PinkFloyd")
  }
}
