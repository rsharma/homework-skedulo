name := "Homework"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.0.0",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "joda-time" % "joda-time" % "2.10.1",
  "org.postgresql" % "postgresql" % "9.3-1102-jdbc41"
)