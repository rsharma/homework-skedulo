# Homework-Skedulo
----------------------

## Question 1: Number Printer
    - Basic question match and check by modulo operator

## Question 2: Order Product
	- Fetch 100 products at a time.
	- To avoid N+1 queries, fetched products by product ids.
	- Since function must be able to handle up to 100 records at a time, the API get maximum of 100 products. 
	
## Question 3: People List
   - Written in ajax to load people from provided implementation.
   - Each APIs get single hit to render the desired output.
   - To run `cd PeopleList` `npm install` `node server.js`

## Question 4: Github user search
   - Implemented in react js
   - Implemented simple in-memory cache using Map (to avoid multiple API calls for same query). It should have better cache implementation. I choosed this approach for brevity.
   - To run `cd GithubSearch` `npm install` and then `yarn start`

## Question 5: The sound of music
   - All events are sorted by start time first
   - Maintained a priority queue based on Priority of events
   - Time quantum is 1 min
   - Preempted when there is higher priority event at same time

### Running ScalaTask Test cases
   - `cd ScalaTasks` `sbt test`