import React, { Component } from 'react'
import axios from 'axios'
import Suggestions from './Suggestions'

const API_URL = 'https://api.github.com/search/users'

//This is just a sample cache. It should have better implementation, predefined size etc.
const cache = new Map()

class Search extends Component {
  state = {
    query: '',
    results: [],
    error: false,
    loading: false
  }

  getInfo = () => {
    if (!cache.has(this.state.query)){
      axios.get(`${API_URL}?q=${this.state.query}`)
      .then(({ data }) => {
        this.setState({
          loading: false,
          results: data.items
        });
        cache.set(this.state.query, data.items)
      },
      (err) => { 
        this.setState({
          loading: false,
          error: true
        })
      })
    } else {
      this.setState({
        loading: false,
        results: cache.get(this.state.query)
      })
    }
  }

  handleInputChange = () => {
    this.setState({
      query: this.search.value
    }, () => {
      if (this.state.query && this.state.query.length > 2) {
          this.setState({
            loading: true
          })
          this.getInfo()
      } else if (!this.state.query) {
        this.setState({
            loading: false,
            results: []
        })
      }
    })
  }

  render() {
    let content;
    if (this.state.loading) {
      content = <div>Loading...</div>;
    } else{
      content = <Suggestions results={this.state.results} />
    }

    if(this.state.error)
      content = <div>Error while fetching users</div>;

    return (
      <form>
        <input id="textboxid"
          placeholder="Search users..."
          ref={input => this.search = input}
          onChange={this.handleInputChange}
        />
        {content}
      </form>
      
    )
  }
}

export default Search