import React from 'react'

//Render a simple table of searched users.
const Suggestions = (props) => {
  const options = props.results.map((r,i) => (
    <tr key={i}>
    	<td><img height="36" width="36" alt="{r.login}" src={r.avatar_url}/></td>
    	<td>{r.login}</td>
    	<td>{r.type}</td>
    	<td>{r.score}</td>
    </tr>
  ))
  return <table><tbody>{options}</tbody></table>
}

export default Suggestions